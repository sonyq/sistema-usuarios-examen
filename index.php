<?php
/*CONFIG*/
require_once('config/Config.php');
/*DISPATCHER*/
require_once('core/Dispatcher.php');
/*View*/
require_once('core/ViewRenderer.php');

session_start();

$action = isset($_GET['action']) ? $_GET['action'] : 'login'; /* si el action esta seteado asigno el valor del get al action y sino es home */

try {
  Dispatcher::$action();
} catch (Throwable $t) {
  //var_dump($t->getMessage());
  Dispatcher::error();
}
