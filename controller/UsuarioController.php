<?php

require_once('model/UsuarioRepository.php');

class UsuarioController extends BaseController {
  protected static $instance;
  protected static $view;

  public function viewUsersList($msg = "") {
    if (SessionController::getInstance()->getUserId()) {
      $user_repo = new UsuarioRepository();
      $users = $user_repo->findAll();
      $params = [
        'users' => $users,
        'msg' => $msg
      ];
      $this::$view->show("Usuarios.php", $params);
    } else {
      $this->redirectLogin();
    }
  }

  public function addUser() {
    if (SessionController::getInstance()->getUserId()) { //chekeo si el usuario esta logeado
      if ($this->postElementsCheck(array('apellido', 'nombre', 'email', 'password', 're_pass'))) { //verifico que esten todos los parametros pasados por post
        $this->prepareData(array('apellido', 'nombre', 'email', 'password', 're_pass')); //elimino cualquier codigo malicioso
        $err = $this->isValidForm($_POST['nombre'], $_POST['apellido'], $_POST['email'], $_POST['password'], $_POST['re_pass']); // valido que los datos cumplan con los requisitos del formulario
        if (empty($err)) {
          $user_repo = new UsuarioRepository();
          if ($user_repo->checkEmail($_POST['email'])) {
            $user_repo->insert([
              'nombre' => $_POST['nombre'],
              'apellido' => $_POST['apellido'],
              'contrasenia' => $_POST['password'],
              'email' => $_POST['email']
            ]);
            $this->viewUsersList('El usuario fue agregado exitosamente');
          } else {
            $this->viewUsersList('Se produjo un error: el email ingresado ya existe');
          }
        } else {
          $this->viewUsersList($err);
        }
      } else {
        $this->viewUsersList('Se produjo un error: faltó completar alguno/s de los datos.');
      }
    } else { //no es un usuario logueado
      $this->redirectLogin();
    }
  }

  public function deleteUser() {
    if (SessionController::getInstance()->getUserId()) {
      if ($this->postElementsCheck(array('user_id'))) {
        $this->prepareData(array('user_id'));
        if ($_POST['user_id'] != SessionController::getInstance()->getUserId()) {
          $user_repo = new UsuarioRepository();
          $user_repo->delete(['id' => $_POST['user_id']]);
          $this->viewUsersList('El usuario fue eliminado');
        } else {
          $this->viewUsersList('Se produjo un error: no puedes eliminar a ese usuario');
        }
      } else {
        $this->viewUsersList('Se produjo un error: no se pudo eliminar ese usuario');
      }
    } else { //no es un usuario logueado
      $this->redirectLogin();
    }
  }

  public function updateUser() {
    if (SessionController::getInstance()->getUserId()) {
      $values = [];
      if ($this->postElementsCheck(array('apellido', 'nombre', 'email', 'user_id'))) {
        if ($this->postElementsCheck(array('password'))) { //verifico si el usuario desea modificar la password
          if ($this->postElementsCheck(array('re_pass'))) {
            $this->prepareData(array('apellido', 'nombre', 'email', 'password', 're_pass'));
            $err = $this->isValidForm($_POST['nombre'], $_POST['apellido'], $_POST['email'], $_POST['password'], $_POST['re_pass']);
            $values['contrasenia'] = $_POST['password'];
          } else {
            $err = 'Se produjo un error: faltó completar alguno/s de los datos.';
          }
        } else { //En caso de no querer modificar la password, actualizo solo lo necesario
          $this->prepareData(array('apellido', 'nombre', 'email'));
          $err = $this->isValidForm($_POST['nombre'], $_POST['apellido'], $_POST['email']);
        }
        if (empty($err)) {
          $user_repo = new UsuarioRepository();
          if ($user_repo->checkEmail($_POST['email'], $_POST['user_id'])) {
            $values += [
              'nombre' => $_POST['nombre'],
              'apellido' => $_POST['apellido'],
              'email' => $_POST['email']
            ];
            $user_repo->update($values, ['id' => $_POST['user_id']]);
            $this->viewUsersList('El usuario se actualizó exitosamente');
          } else {
            $this->viewUsersList('Se produjo un error: el email ingresado ya existe');
          }
        } else { //error con algun campo del form
          $this->viewUsersList($err);
        }
      } else {
        $this->viewUsersList('Se produjo un error: faltó completar alguno/s de los datos.');
      }
    } else {
      $this->redirectLogin();
    }
  }

  function isValidForm($name, $last_name, $email, $password = null, $re_pass = null) {
    $err = '';
    if (!(preg_match("/^[a-z ñáéíóú]{2,60}+$/i", $name))) {
      $err .= 'error en nombre: se permiten solo letras, espacios y acentos. Mínimo 2 caracteres, máximo 60.<br> ';
    }
    if (!(preg_match("/^[a-z ñáéíóú]{2,60}+$/i", $last_name))) {
      $err .= 'error en apellido: se permiten solo letras, espacios y acentos. Mínimo 2 caracteres, máximo 60.<br> ';
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL) === true) {
      $err .= 'error en email: no es un email válido.<br>';
    }
    if (!is_null($password) && ((strlen($password) < 4) || (strlen($password) > 20))) {
      $err .= 'error en password: mínimo 8 caracteres, máximo 20.<br> ';
    }
    if ($password != $re_pass) {
      $err .= 'error: password y confirmación deben coincidir.<br> ';
    }
    return $err;
  }
}
