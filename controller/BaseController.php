<?php

abstract class BaseController {
  protected static $instance;
  protected static $view;

  private function __construct() {
  }

  final protected function __clone() {
  }

  public static function getInstance() {
    if (!static::$instance instanceof static) {
      static::$instance = new static();
      static::$view = new ViewRenderer();
    }
    return static::$instance;
  }

  public function redirectLogin() {
    header("Location: ./?action=login", true, 301);
  }

  public function redirectHome() {
    header("Location: ./?action=home", true, 301);
  }

  public function postElementsCheck($elements) {
    $i = 0;
    $i_max = count($elements);
    $ok = ($i < $i_max);
    while ($ok && $i < $i_max) {
      $key = $elements[$i++];
      $ok = (isset($_POST[$key]) && (!empty($_POST[$key]) || is_numeric($_POST[$key])));
    }

    return $ok;
  }

  public function prepareData($elements) {
    foreach ($elements as $key) {
      if (isset($_POST[$key])) {
        $_POST[$key] = trim($_POST[$key]); //Elimina espacio en blanco (u otro tipo de caracteres) del inicio y el final de la cadena
        $_POST[$key] = strip_tags($_POST[$key]); //Retira las etiquetas HTML y PHP de un string
        $_POST[$key] = addslashes($_POST[$key]); //Escapa un string con barras invertidas
        $_POST[$key] = stripslashes($_POST[$key]); //Quita las barras de un string con comillas escapadas
        $_POST[$key] = htmlspecialchars($_POST[$key]); //Convierte caracteres especiales en entidades HTML
      }
    }
  }
}
