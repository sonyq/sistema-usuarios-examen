<?php

require_once('model/UsuarioRepository.php');

class SessionController extends BaseController {
  protected static $instance;
  protected static $view;

  public function startUserSession($user) {
    session_destroy();
    session_set_cookie_params(0);
    session_start();
    $_SESSION['id'] = $user['id'];
    $_SESSION['email'] = $user['email'];
  }

  public function endUserSession() {
    session_destroy();
    $_SESSION = array();
  }

  public function getUserId() {
    if (array_key_exists('id', $_SESSION)) {
      return $_SESSION['id'];
    }
    return false;
  }

  public function getUser() {
    if ($this->getUserId()) {
      $user_repo = new UsuarioRepository();
      return $user_repo->findBy(['id' => $this->getUserId()])[0];
    }
    return false;
  }

  function viewLogin($error = "") {
    $_GET['action'] = '';
    if ($this->getUserId()) { //chequeo que no este logeado
      $this->redirectHome();
    } else {
      $this::$view->show("Login.php", array('msg' => $error));
    }
  }

  public function login() {

    if ($this->getUserId()) {
      return $this->redirectHome();
    }
    if ($this->postElementsCheck(array('email', 'password'))) { //check de los parametros pasados por post
      $user_repo = new UsuarioRepository();
      $user = $user_repo->findBy(['email' => $_POST['email'], 'contrasenia' => $_POST['password']]);
      if (!empty($user)) { //si el usuario existe lo logeo
        $this->startUserSession($user[0]);
        $this->redirectHome();
        $_GET['action'] = '';
        return;
      } else {
        $this->viewLogin("Email o Contraseña incorrecta");
      }
    } else {
      $this->viewLogin("Email o Contraseña incorrecta");
    }
  }

  public function logout() {
    if ($this->getUserId()) {
      $this->endUserSession();
    }
    $this->redirectLogin();
    $_GET['action'] = '';
  }
}
