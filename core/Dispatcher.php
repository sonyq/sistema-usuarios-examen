<?php

/*CONTROLLERS*/
require_once('controller/BaseController.php');
require_once('controller/SessionController.php');
require_once('controller/UsuarioController.php');
require_once('controller/ErrorController.php');

class Dispatcher {

    static function login() {
        SessionController::getInstance()->viewLogin();
    }

    static function login_check() {
        SessionController::getInstance()->login();
    }

    static function logout() {
        SessionController::getInstance()->logout();
    }

    static function error() {
        ErrorController::getInstance()->error();
    }

    static function home() {
        UsuarioController::getInstance()->viewUsersList();
    }

    static function add_user() {
        UsuarioController::getInstance()->addUser();
    }

    static function update_user() {
        UsuarioController::getInstance()->updateUser();
    }

    static function delete_user() {
        UsuarioController::getInstance()->deleteUser();
    }
}
