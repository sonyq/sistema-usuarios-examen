<?php

abstract class View {

    private static $view;

    public static function getView() {

        if (!isset(self::$view)) {
            self::$view = new static();
        }
        return self::$view;
    }

    public function render($view, $params = array()) {
        $base_template = file_get_contents(BASE_TEMPLATE);
        $base_template = $this->entryScript($base_template);
        $base_template = $this->stylesheets($base_template);
        $base_template = $this->loadCustomViews($base_template, $params);

        ob_start();
        require(VIEWS_PATH . $view);

        $output = ob_get_contents();
        ob_end_clean();

        $template = $this->loadView($output, $base_template);

        return $template;
    }

    private function entryScript($base_template) {
        return str_replace('{{entry_script}}', APP_JS, $base_template);
    }

    private function stylesheets($base_template) {
        return str_replace('{{stylesheets}}', STYLES_CSS, $base_template);
    }

    private function loadView($view, $base_template) {
        return str_replace('{{view}}', $view, $base_template);
    }

    private function loadCustomViews($template, $params) {

        preg_match_all('/\{(Load)\}(.+?)\{\/\\1\}/', $template, $views);
        if (!empty($views[0])) {
            ob_start();
            foreach ($views[2] as $i => $v) {
                require(VIEWS_PATH . $v . '.php');
                $output = ob_get_clean();
                $template = str_replace($views[0][$i], $output, $template);
            }
        }

        return $template;
    }
}
