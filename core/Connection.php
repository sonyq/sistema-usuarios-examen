<?php

abstract class Connection {

  protected $conn;

  function __construct() {
    $this->getConnection();
  }

  public function getConnection() {
    $this->conn = new PDO("pgsql:host=" . DB_HOST . ";port=" . DB_PORT . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);
  }
}
