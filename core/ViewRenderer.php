<?php

require_once('core/View.php');
require_once('model/UsuarioRepository.php');

class ViewRenderer extends View {

  public function show($view, $params = array()) {
    $params['loged_user'] = SessionController::getInstance()->getUser();
    echo self::getView()->render($view, $params);
  }
}
