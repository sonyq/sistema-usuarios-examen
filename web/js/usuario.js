
let b_new = document.getElementById('b_new');
b_new.setAttribute("onclick", "document.getElementById('new_user_modal').style.display='block'");

let b_edits = document.querySelectorAll('.b_edit');
for (var i = 0; i < b_edits.length; i++) {
  b_edits[i].setAttribute("onclick", "editUser(" + b_edits[i].id + ")");
}

let b_deletes = document.querySelectorAll('.b_delete');
for (var i = 0; i < b_deletes.length; i++) {
  b_deletes[i].setAttribute("onclick", "deleteUser(" + b_deletes[i].id + ")");
}

function editUser(user_id) {
  let data = document.getElementById('user_' + user_id).cells;
  document.getElementById('edit_nom').setAttribute('value', data[1].innerHTML);
  document.getElementById('edit_ap').setAttribute('value', data[2].innerHTML);
  document.getElementById('edit_em').setAttribute('value', data[3].innerHTML);
  document.getElementById('edit_id').setAttribute('value', user_id);
  document.getElementById('edit_user_modal').style.display = 'block';
}

function deleteUser(user_id) {
  document.getElementById('delete_id').setAttribute('value', user_id);
  document.getElementById('delete_user_modal').style.display = 'block';
}
