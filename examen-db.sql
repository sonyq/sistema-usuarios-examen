-- public.usuario definition

CREATE TABLE usuario (
	id serial NOT NULL,
	nombre varchar(80) NOT NULL,
	apellido varchar(80) NOT NULL,
	email varchar(80) NOT NULL,
	contrasenia varchar(80) NOT NULL,
	CONSTRAINT usuario_email_key UNIQUE (email),
	CONSTRAINT usuario_pkey PRIMARY KEY (id)
);

INSERT INTO usuario (nombre,apellido,email,contrasenia) VALUES
     ('pepe','fulano','test@test.com','1234'),
	 ('mengano','fulano','test1@test.com','12345'),
	 ('Mariano Gaston','Cordoba','test2@test.com','1234');