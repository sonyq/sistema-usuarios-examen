<?php

require_once('model/BaseRepository.php');

class UsuarioRepository extends BaseRepository {

  function __construct() {

    parent::__construct();
  }

  public function table() {
    return 'usuario';
  }

  public function checkEmail($email, $ignore_user_id = -1) {
    $query = $this->conn->prepare("SELECT * FROM usuario WHERE email=:email AND id <> :id");
    $query->bindParam(":email", $email);
    $query->bindParam(":id", $ignore_user_id);
    $query->execute();
    $valid = $query->fetchAll();
    return empty($valid);
  }
}
