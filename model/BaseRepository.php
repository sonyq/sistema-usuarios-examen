<?php

require_once('core/Connection.php');

abstract class BaseRepository extends Connection {

    abstract protected function table();

    function __construct() {

        parent::__construct();
    }

    public function findAll() {
        $query = "SELECT * FROM " . $this->table();
        $query = $this->conn->prepare($query);
        $query->execute();
        return $query->fetchall();
    }

    public function findBy($params) {
        $where = $this->buildWhere($params);
        $query = "SELECT * FROM " . $this->table() . $where;
        $query = $this->conn->prepare($query);
        $query->execute(array_values($params));
        return $query->fetchall();
    }

    public function insert($params) {
        $insert = $this->buildInsert($params);
        $query = "INSERT INTO " . $this->table() . $insert;
        $query = $this->conn->prepare($query);
        $query->execute(array_values($params));
    }

    public function update($values, $params) {
        $where = $this->buildWhere($params);
        $update = $this->buildUpdate($values);
        $query = "UPDATE " . $this->table() . $update . $where;
        $query = $this->conn->prepare($query);
        $params = array_merge($values, $params);
        $query->execute(array_values($params));
    }

    public function delete($params) {
        $where = $this->buildWhere($params);
        $query = "DELETE FROM " . $this->table() . $where;
        $query = $this->conn->prepare($query);
        $query->execute(array_values($params));
        return $query->fetchall();
    }

    private function buildWhere($params) {
        $where = "";
        foreach ($params as $key => $value) {
            $where = $where . $key . " = ? AND ";
        }
        $where = trim($where, "AND ");
        $where = $where = " WHERE " . $where;
        return !empty($params) ? $where : "";
    }

    private function buildUpdate($params) {
        $keys = array_keys($params);
        $fields = implode(' = ?, ', $keys);

        $update = " SET " . $fields . "= ? ";
        return !empty($params) ? $update : "";
    }

    private function buildInsert($params) {
        $keys = array_keys($params);
        $fields = implode(', ', $keys);
        $placeholder = substr(str_repeat('?,', count($keys)), 0, -1);

        $insert = "(" . $fields . ") VALUES(" . $placeholder . ")";
        return !empty($params) ? $insert : "";
    }
}
