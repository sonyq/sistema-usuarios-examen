<?php
/*VIEW CONFIG*/
const BASE_TEMPLATE = 'View/template/Base.html';
const VIEWS_PATH = 'View/';
const APP_JS = './web/js/app.js';
const STYLES_CSS = './web/css/styles.css';
/* DB CONFIG */
const DB_USERNAME = "postgres";
const DB_PASSWORD = "";
const DB_HOST ="127.0.0.1";
const DB_PORT = "5432";
const DB_NAME = "postgres";