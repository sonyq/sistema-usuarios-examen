<div class="table-conteiner centered">
  <table>
    <caption>
      <h3>Lista de usuarios</h3>
    </caption>
    <thead>
      <tr>
        <td>&nbsp;</td>
        <td>Nombre</td>
        <td>Apellido</td>
        <td>Email</td>
        <td><button id='b_new'>Nuevo usuario</button></td>
      </tr>
    </thead>
    <tbody>
      <?php
      $tbody = "";
      foreach ($params["users"] as $i => $u) {
        $tbody = $tbody . "
        <tr id='user_{$u['id']}'>
          <th>{$i}</th>
          <td>{$u['nombre']}</td>
          <td>{$u['apellido']}</td>
          <td>{$u['email']}</td>
          <td><button id='{$u['id']}' class='b_edit'>Editar</button> <button id='{$u['id']}' class='b_delete'>Borrar</button></td>
        </tr>
        ";
      }
      echo ($tbody);
      ?>
    </tbody>
  </table>

  <!--  modal nuevo usuario -->
  <div id="new_user_modal" class="modal modal-form">
    <form class="modal-content" action="./?action=add_user" method="post">
      <h1>Nuevo usuario</h1>
      <hr>
      <label for="nombre"><b>Nombre</b></label>
      <input type="text" name="nombre" required>
      <label for="apellido"><b>Apellido</b></label>
      <input type="text" name="apellido" required>
      <label for="email"><b>Email</b></label>
      <input type="text" name="email" required>
      <label for="password"><b>Contraseña</b></label>
      <input type="password" name="password" required>
      <label for="re_pass"><b>Confirmar contraseña</b></label>
      <input type="password" name="re_pass" required>

      <div class="clearfix">
        <button type="submit">Crear</button>
        <button type="button" class="cancel" onclick="document.getElementById('new_user_modal').style.display='none'">Cancelar</button>
      </div>
    </form>
  </div>
  <!--  modal nuevo usuario -->

  <!--  modal editar usuario -->
  <div id="edit_user_modal" class="modal modal-form">
    <form class="modal-content" action="./?action=update_user" method="post">
      <h1>Editar usuario</h1>
      <hr>
      <label for="nombre"><b>Nombre</b></label>
      <input id="edit_nom" type="text" name="nombre" required>
      <label for="apellido"><b>Apellido</b></label>
      <input id="edit_ap" type="text" name="apellido" required>
      <label for="email"><b>Email</b></label>
      <input id="edit_em" type="text" name="email" required>
      <label for="password"><b>Contraseña</b></label>
      <input type="password" name="password">
      <label for="re_pass"><b>Confirmar contraseña</b></label>
      <input type="password" name="re_pass">
      <input id="edit_id" name="user_id" type="hidden">

      <div class="clearfix">
        <button type="submit">Editar</button>
        <button type="button" class="cancel" onclick="document.getElementById('edit_user_modal').style.display='none'">Cancelar</button>
      </div>
    </form>
  </div>
  <!--  modal editar usuario -->

  <!--  modal borrar usuario -->
  <div id="delete_user_modal" class="modal">
    <form class="modal-content" action="./?action=delete_user" method="post">
      <div class="container">
        <h1>Borrar usuario</h1>
        <hr>
        <p>Seguro que desea borrar ese usuario?</p>
        <input id="delete_id" name="user_id" type="hidden">

        <div class="clearfix">
          <button type="submit">Borrar</button>
          <button type="button" class="cancel" onclick="document.getElementById('delete_user_modal').style.display='none'">Cancelar</button>
        </div>
      </div>
    </form>
  </div>
  <!--  modal borrar usuario -->
</div>
<script type="text/javascript" src="./web/js/usuario.js"></script>