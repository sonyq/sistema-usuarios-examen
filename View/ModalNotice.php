<!--  modal notice -->
<div id="notice_modal" class="modal">
  <h1>Aviso</h1>
  <hr>
  <?php
  if (isset($params['msg']) && !empty($params['msg'])) {
    echo ("<p id='msg'>" . $params['msg'] . "</p>");
  }
  ?>
  <div class="clearfix">
    <button type="submit" onclick="document.getElementById('notice_modal').style.display='none'">Ok</button>
  </div>
</div>
<!--  modal notice -->