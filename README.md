# Examen 
Sistema de gestión de usuarios

### Enunciado

Realizar un crud de usuarios junto a un formulario de login.
Solo se utilizara PHP, JS y CSS, sin otros frameworks o librerías del lenguaje.

### Suposiciones

_Algunas de las suposiciones que hice en cuanto al enunciado son_

El email de cada usuario es único y este será el que utilizaran para poder logearse.
Un usuario no puede eliminarse a si mismo.
Un usuario puede editarse a si mismo y otros, incluso cambiar su clave si lo desea.

### Instalación

_Clonar_

```
git clone https://sonyq@bitbucket.org/sonyq/sistema-usuarios-examen.git
```

_Script DataBase_

```
Crear un base de datos junto a un schema
Ejecutar el script examen-db.sql
Cambiar la configuración en /config/Config.php

```

_Acceso al sistema_

```
Utilizar las siguientes credenciales
Email: test@test.com
Password: 1234

```

### Aclaraciones

Para el diseño del sistema opte por un clásico MVC.
Para la vista implemente un pequeño renderizador de plantillas (Core/View.php),
el cual me permite utilizar custom tags para importar otras vistas, estilos 
y scripts. Esto facilita mucho la modularizacion y reutilización de los
componentes de la vista.
Las contraseñas de los usuarios las almacene como texto plano por simplicidad
para el ejercicio.